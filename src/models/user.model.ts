import { Entity, model, property, hasMany } from '@loopback/repository';
import { ThingAccess } from './thing-access.model';
import { Role, UserRole } from '.';

@model({
  settings: {
    indexes: {
      uniqueEmail: {
        keys: {
          email: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
})
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id: string;

  @property({
    type: 'string',
    generated: false,
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  lastname: string;

  @property({
    type: 'string',
    required: true,
  })
  firstname: string;

  @property({
    type: 'string',
    required: true,
  })
  password: string;

  @hasMany(() => ThingAccess, { keyTo: 'userId' })
  thingsAccesses: ThingAccess[];

  @hasMany(() => UserRole, { keyTo: 'userId' })
  roles: UserRole[];

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
