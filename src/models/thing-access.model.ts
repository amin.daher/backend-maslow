import { Entity, model, property, belongsTo } from '@loopback/repository';
import { User } from '.';

@model()
export class ThingAccess extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  thing_uid: string;

  @property({
    type: 'date',
    required: true,
  })
  start_date: string;

  @property({
    type: 'date',
    required: true,
  })
  end_date: string;

  @belongsTo(() => User)
  userId: string;

  constructor(data?: Partial<ThingAccess>) {
    super(data);
  }
}

export interface ThingAccessRelations {
  // describe navigational properties here
}

export type ThingAccessWithRelations = ThingAccess & ThingAccessRelations;
