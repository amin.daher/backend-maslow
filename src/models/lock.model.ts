import { Entity, model, property } from '@loopback/repository';

@model({
  settings: {
    indexes: {
      unique_constructor_id: {
        keys: {
          constructor_lock_id: 1,
        },
        options: {
          unique: true,
        },
      },
    },
  },
}
)
export class Lock extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'string',
    required: true,
  })
  brand: string;

  @property({
    type: 'string',
    required: true,
  })
  //Id from constructor
  constructor_lock_id: string;

  constructor(data?: Partial<Lock>) {
    super(data);
  }
}

export interface LockRelations {
  // describe navigational properties here
}

export type LockWithRelations = Lock & LockRelations;
