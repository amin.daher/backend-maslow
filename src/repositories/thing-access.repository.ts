import { DefaultCrudRepository, BelongsToAccessor, repository } from '@loopback/repository';
import { ThingAccess, ThingAccessRelations, User } from '../models';
import { MongodbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { UserRepository } from '.';

export class ThingAccessRepository extends DefaultCrudRepository<ThingAccess, typeof ThingAccess.prototype.id, ThingAccessRelations> {


  public readonly user: BelongsToAccessor<User, typeof ThingAccess.prototype.id>;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @repository.getter('UserRepository')
    getUserRepository: Getter<UserRepository>
  ) {
    super(ThingAccess, dataSource);
    this.user = this.createBelongsToAccessorFor('user', getUserRepository);
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }
}
