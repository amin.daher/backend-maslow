import { DefaultCrudRepository, HasManyRepositoryFactory, repository } from '@loopback/repository';
import { User, UserRelations, ThingAccess, UserRole } from '../models';
import { MongodbDataSource } from '../datasources';
import { inject, Getter } from '@loopback/core';
import { ThingAccessRepository } from './thing-access.repository';
import { UserRoleRepository } from '.';

export class UserRepository extends DefaultCrudRepository<User, typeof User.prototype.id, UserRelations> {

  public readonly thingsAccesses: HasManyRepositoryFactory<ThingAccess, typeof User.prototype.id>;
  public readonly roles: HasManyRepositoryFactory<UserRole, typeof User.prototype.id>;

  constructor(
    @inject('datasources.mongodb') dataSource: MongodbDataSource,
    @repository.getter('ThingAccessRepository')
    getThingAccessRepository: Getter<ThingAccessRepository>,
    @repository.getter('UserRoleRepository')
    getUserRoleRepository: Getter<UserRoleRepository>
  ) {
    super(User, dataSource);

    this.thingsAccesses = this.createHasManyRepositoryFactoryFor('thingsAccesses', getThingAccessRepository);
    this.roles = this.createHasManyRepositoryFactoryFor('roles', getUserRoleRepository);

    this.registerInclusionResolver('thingsAccesses', this.thingsAccesses.inclusionResolver);
    this.registerInclusionResolver('roles', this.roles.inclusionResolver);
  }
}

export type Credentials = {
  email: string;
  password: string;
};
