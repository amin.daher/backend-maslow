import { HttpErrors } from '@loopback/rest';
import { UserRepository, Credentials } from '../repositories/user.repository';
import { User } from '../models/user.model';
import { UserService } from '@loopback/authentication';
import { UserProfile } from '@loopback/security';
import { repository, Filter } from '@loopback/repository';
import { PasswordHasher } from './hash-password.service';
import { PasswordHasherBindings } from '../keys';
import { inject } from '@loopback/context';

export class MyUserService implements UserService<User, Credentials> {
  constructor(
    @repository(UserRepository) public userRepository: UserRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher,
  ) { }

  async verifyCredentials(credentials: Credentials): Promise<User> {
    const invalidCredentialsError = 'Invalid email or password';

    const foundUser = await this.userRepository.findOne({
      where: { email: credentials.email }
    });

    if (!foundUser) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }
    const passwordMatched = await this.passwordHasher.comparePassword(credentials.password, foundUser.password);

    if (!passwordMatched) {
      throw new HttpErrors.Unauthorized(invalidCredentialsError);
    }

    return foundUser;
  }

  convertToUserProfile(user: User): UserProfile {
    delete user.password;
    const userProfile: any = { ...user, ['securityId']: user.email };
    
    return userProfile;
  }
}
