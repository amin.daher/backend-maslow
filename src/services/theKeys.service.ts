import { bind, /* inject, */ BindingScope } from '@loopback/core';
import {
  THEKEYS_SERVER_HOST, THEKEYS_USERNAME,
  THEKEYS_PASSWORD, THEKEYS_AUTH_PATH, THEKEYS_LOCK_CLOSE_PATH,
  THEKEYS_LOCK_OPEN_PATH, THEKEYS_LOCK_GET_INFOS_PATH
} from '../constants/theKeys.constants';
import request from 'request';
import { LockService } from './lock.service';
import { HttpMethod } from '../util/HttpMethod';

@bind({
  scope: BindingScope.TRANSIENT,
  tags: {
    key: 'services.theKeys'
  }
})
export class TheKeysService implements LockService {
  constructor() { }

  async getLockInfos(lockId: string): Promise<any[]> {
    const token = await this.getToken();
    
    return this.callTheKeys(THEKEYS_LOCK_GET_INFOS_PATH + lockId, HttpMethod.GET, token);
  }

  async openLock(lockId: string): Promise<any[]> {
    const token = await this.getToken();

    return this.callTheKeys(THEKEYS_LOCK_OPEN_PATH + lockId, HttpMethod.POST, token);
  }

  async closeLock(lockId: string): Promise<any[]> {
    const token = await this.getToken();

    return this.callTheKeys(THEKEYS_LOCK_CLOSE_PATH + lockId, HttpMethod.POST, token);
  }

  async getToken() {
    var conResult = JSON.parse(await this.getAccessToken(THEKEYS_USERNAME, THEKEYS_PASSWORD));

    return conResult.token;
  }

  async callTheKeys(path: string, method: HttpMethod, token: string): Promise<any> {

    return new Promise(async function (resolve, reject) {
      const headers = {
        Authorization: 'Bearer ' + token
      }
      if (method === HttpMethod.POST) {
        request.post({ url: THEKEYS_SERVER_HOST + path, headers: headers }, function optionalCallback(err: any, httpResponse: any, body: any) {
          if (err) {
            reject(err);
          }
          resolve(JSON.parse(body));
        });
      }
      else {
        request.get({ url: THEKEYS_SERVER_HOST + path, headers: headers }, function optionalCallback(err: any, httpResponse: any, body: any) {
          if (err) {
            reject(err);
          }
          resolve(JSON.parse(body));
        });
      }

    });
  }

  async getAccessToken(username: string, password: string): Promise<any> {

    return new Promise(async function (resolve, reject) {
      const formData = { _username: username, _password: password };
      request.post({ url: THEKEYS_SERVER_HOST + THEKEYS_AUTH_PATH, formData: formData }, function optionalCallback(err: any, httpResponse: any, body: any) {
        if (err) {
          reject(err);
        }
        resolve(body);
      });
    });
  }
}
