import { bind, /* inject, */ BindingScope } from '@loopback/core';
import { OPENHAB_SERVER_HOST, OPENHAB_SERVER_PORT, THINGS_RESOURCE, ITEMS_RESOURCE } from '../constants/openhab.constants';
import * as http from 'http';
import { HttpMethod } from '../util/HttpMethod';

@bind({
  scope: BindingScope.TRANSIENT,
  tags: {
    key: 'services.openhab'
  }
})

export class OpenhabService {
  constructor() { }

  async getAllThings(): Promise<any[]> {

    return this.callOpenhab(THINGS_RESOURCE, HttpMethod.GET);
  }

  async getThing(thingUid: string): Promise<any[]> {

    return this.callOpenhab(THINGS_RESOURCE + thingUid, HttpMethod.GET);
  }

  async getItem(itemName: string): Promise<any[]> {

    return this.callOpenhab(ITEMS_RESOURCE + itemName, HttpMethod.GET);
  }

  async updateItem(itemName: string, itemValue: string): Promise<any[]> {

    return this.callOpenhab(ITEMS_RESOURCE + itemName, HttpMethod.POST, itemValue);
  }

  async callOpenhab(path: string, method: HttpMethod, body?: string): Promise<any> {

    return new Promise(async function (resolve, reject) {
      const options = {
        hostname: OPENHAB_SERVER_HOST,
        port: OPENHAB_SERVER_PORT,
        path: path,
        method: method,
        headers: {}
      };

      if (body) {
        options.headers = {
          'Content-Length': body.length
        };
      }

      const req = http.request(options, res => {
        res.on('data', data => {
          resolve(JSON.parse(data));
        });
        res.on('end', () => {
          resolve();
        });
      });

      req.on('error', error => {
        console.error('Error while executing ' + method + ' on ' + path);

        return { error: error.message };
      });

      if (body) {
        req.write(body);
      }

      req.end();
    });
  }
}
