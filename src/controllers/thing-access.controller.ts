import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository';
import { post, param, get, getModelSchemaRef, patch, put, del, requestBody } from '@loopback/rest';
import { ThingAccess } from '../models';
import { ThingAccessRepository } from '../repositories';
import { authenticate } from '@loopback/authentication';
import { authorize } from '@loopback/authorization';

export class ThingAccessController {
  constructor(
    @repository(ThingAccessRepository)
    public thingAccessRepository: ThingAccessRepository,
  ) { }

  @post('/thing-accesses', {
    responses: {
      '200': {
        description: 'ThingAccess model instance',
        content: { 'application/json': { schema: getModelSchemaRef(ThingAccess) } },
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ThingAccess, {
            title: 'NewThingAccess',
            exclude: ['id'],
          }),
        },
      },
    })
    thingAccess: Omit<ThingAccess, 'id'>,
  ): Promise<ThingAccess> {

    return this.thingAccessRepository.create(thingAccess);
  }

  @get('/thing-accesses', {
    responses: {
      '200': {
        description: 'Array of ThingAccess model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ThingAccess, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async find(
    @param.filter(ThingAccess) filter?: Filter<ThingAccess>,
  ): Promise<ThingAccess[]> {

    return this.thingAccessRepository.find(filter);
  }

  @get('/thing-accesses/{id}', {
    responses: {
      '200': {
        description: 'ThingAccess model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ThingAccess, { includeRelations: true }),
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(ThingAccess, { exclude: 'where' }) filter?: FilterExcludingWhere<ThingAccess>
  ): Promise<ThingAccess> {

    return this.thingAccessRepository.findById(id, filter);
  }

  @del('/thing-accesses/{id}', {
    responses: {
      '204': {
        description: 'ThingAccess DELETE success',
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async deleteById(@param.path.string('id') id: string): Promise<void> {

    await this.thingAccessRepository.deleteById(id);
    
  }
}
