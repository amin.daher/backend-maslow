import { get, param, requestBody, post } from '@loopback/openapi-v3';
import { OpenhabService } from '../services';
import { inject } from '@loopback/context';
import { authenticate } from '@loopback/authentication';
import { SecurityBindings, UserProfile } from '@loopback/security';
import { repository } from '@loopback/repository';
import { UserRepository, UserRoleRepository, ThingAccessRepository } from '../repositories';

export class ThingController {

  constructor(
    @inject('services.openhab') private openhabService: OpenhabService,
    @repository(UserRoleRepository) private userRoleRepository: UserRoleRepository,
    @repository(ThingAccessRepository) private thingAccessRepository: ThingAccessRepository,
  ) {}

  @get('/things')
  @authenticate('jwt')
  async find(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile
  ): Promise<any[]> {
    const allThings = await this.openhabService.getAllThings();
    const roles =
      await this.userRoleRepository.find({
        where: { userId: { like: currentUserProfile.id } }
      });

    const userRoles = roles.map(role => role.roleId);
    
    if (userRoles.includes('ADMIN') || userRoles.includes('EMERGENCY')) {

      return allThings;
    }
    else {

      const thingsAccesses =
        await this.thingAccessRepository.find({ where: { userId: { like: currentUserProfile.id } } });

      //Get valables accesses
      const valableAccesses = thingsAccesses.filter(access =>
        new Date(access.end_date) > new Date() && new Date(access.start_date) < new Date());
      const valableAccessesThings = valableAccesses.map(access => access.thing_uid);

      const accessibleThings = allThings.filter(thing => valableAccessesThings.includes(thing.UID))

      return accessibleThings;
    }
  }

  @get('/things/{thingUid}')
  @authenticate('jwt')
  async findByUid(
    @param.path.string('thingUid') thingUid: string
  ): Promise<any> {

    return this.openhabService.getThing(thingUid);
  }

  @get('/items/{itemName}')
  @authenticate('jwt')
  async findItemByName(
    @param.path.string('itemName') itemName: string
  ): Promise<any> {

    return this.openhabService.getItem(itemName);
  }

  @post('/items/{itemName}')
  @authenticate('jwt')
  async updateItem(
    @param.path.string('itemName') itemName: string,
    @requestBody() body: { itemValue: number | OpenhabState }
  ): Promise<any> {

    return this.openhabService.updateItem(itemName, body.itemValue.toString());
  }
}

export enum OpenhabState {
  ON = 'ON',
  OFF = 'OFF',
  CLOSE = 'CLOSE',
  OPEN = 'OPEN'
}
