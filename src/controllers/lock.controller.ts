import { get, param, requestBody, post } from '@loopback/openapi-v3';
import { inject } from '@loopback/context';
import { TheKeysService } from '../services/theKeys.service';
import { repository } from '@loopback/repository';
import { LockRepository } from '../repositories';

//Serrure TheKeys
export class LocksController {
  constructor(
    @inject('services.theKeys') private theKeysService: TheKeysService,
    @repository(LockRepository) private lockRepository: LockRepository,
  ) {}

  @get('/locks')
  async getLocks(): Promise<any> {

    return this.lockRepository.find();
  }

  @get('/locks/{lockId}')
  async getLockById(
    @param.path.string('lockId') lockId: string
  ): Promise<any> {

    const lock = await this.lockRepository.findById(lockId);

    return this.theKeysService.getLockInfos(lock.constructor_lock_id);
  }

  @post('/locks/open/{lockId}')
  async openByUid(
    @param.path.string('lockId') lockId: string
  ): Promise<any> {

    const lock = await this.lockRepository.findById(lockId);

    return this.theKeysService.openLock(lock.constructor_lock_id);
  }

  @post('/locks/close/{lockId}')
  async closeByUid(
    @param.path.string('lockId') lockId: string
  ): Promise<any> {

    const lock = await this.lockRepository.findById(lockId);

    return this.theKeysService.closeLock(lock.constructor_lock_id);
  }
}
