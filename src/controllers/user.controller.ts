import { Count, CountSchema, Filter, FilterExcludingWhere, repository, Where } from '@loopback/repository';
import { post, param, get, getModelSchemaRef, patch, requestBody, HttpErrors } from '@loopback/rest';
import { User } from '../models';
import { UserRepository, UserRoleRepository, Credentials, ThingAccessRepository } from '../repositories';
import { promisify } from 'util';
import { inject } from '@loopback/core';
import { TokenServiceBindings, UserServiceBindings, PasswordHasherBindings } from '../keys';
import { MyUserService } from '../services/user.service';
import { CredentialsRequestBody } from './specs/user.controller.specs';
import { PasswordHasher } from '../services/hash-password.service';
import { TokenService, authenticate } from '@loopback/authentication';
import { OPERATION_SECURITY_SPEC } from '../utils/security.specs';
import { SecurityBindings, UserProfile } from '@loopback/security';
import { authorize } from '@loopback/authorization';

export class UserController {
  constructor(
    @repository(UserRepository) private userRepository: UserRepository,
    @repository(UserRoleRepository) private userRoleRepository: UserRoleRepository,
    @repository(ThingAccessRepository) private thingAccessRepository: ThingAccessRepository,
    @inject(TokenServiceBindings.TOKEN_SERVICE)
    public jwtService: TokenService,
    @inject(UserServiceBindings.USER_SERVICE)
    public userService: MyUserService,
    @inject(PasswordHasherBindings.PASSWORD_HASHER)
    public passwordHasher: PasswordHasher
  ) {}

  @post('/users', {
    responses: {
      '200': {
        description: 'User',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User
            }
          }
        }
      }
    }
  })
  @authenticate('jwt')
  async create(@requestBody() user: User): Promise<User> {

    user.password = await this.passwordHasher.hashPassword(user.password);

    try {
      const savedUser = await this.userRepository.create(user);
      delete savedUser.password;

      return savedUser;
    } catch (error) {

      // Duplicate keys
      if (error.code === 11000) {

        if (error.errmsg.includes('index: uniqueEmail')) {

          throw new HttpErrors.Conflict('Email value is already taken');

        } else {

          throw new HttpErrors.Conflict('Conflict error');

        }
      } else {

        throw error;

      }
    }
  }

  @get('/users', {
    responses: {
      '200': {
        description: 'Array of User model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(User, { includeRelations: true }),
            },
          },
        },
      },
    },
  })
  @authenticate('jwt')
  @authorize({ allowedRoles: ['ADMIN'] })
  async find(
    @param.filter(User) filter?: Filter<User>,
  ): Promise<User[]> {
    const users = await this.userRepository.find(filter);

    for (let i = 0; i < users.length; i++) {

      users[i].roles = await this.userRoleRepository.find({ where: { userId: { like: users[i].id } } });

      users[i].thingsAccesses =
        await this.thingAccessRepository.find({ where: { userId: { like: users[i].id } } });
    }

    return users;
  }

  @patch('/users', {
    responses: {
      '200': {
        description: 'User PATCH success count',
        content: { 'application/json': { schema: CountSchema } },
      },
    },
  })
  @authenticate('jwt')
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, { partial: true }),
        },
      },
    })
    user: User,
    @param.where(User) where?: Where<User>,
  ): Promise<Count> {

    return this.userRepository.updateAll(user, where);
  }

  @get('/users/{id}', {
    responses: {
      '200': {
        description: 'User model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(User, { includeRelations: true }),
          },
        },
      },
    },
  })
  @authenticate('jwt')
  async findById(
    @param.path.string('id') id: string,
    @param.filter(User, { exclude: 'where' }) filter?: FilterExcludingWhere<User>
  ): Promise<User> {
    const findUser = await this.userRepository.findById(id, filter);
    findUser.roles = await this.userRoleRepository.find({ where: { userId: { like: id } } });
    findUser.thingsAccesses =
      await this.thingAccessRepository.find({ where: { userId: { like: findUser.id } } });

    return findUser;
  }

  @patch('/users/{id}', {
    responses: {
      '204': {
        description: 'User PATCH success',
      },
    },
  })
  @authenticate('jwt')
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(User, { partial: true }),
        },
      },
    })
    user: User,
  ): Promise<void> {

    await this.userRepository.updateById(id, user);
    
  }


  @post('users/login', {
    responses: {
      '200': {
        description: 'Token',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                token: {
                  type: 'string'
                }
              }
            }
          }
        }
      }
    }
  })
  async login(@requestBody(CredentialsRequestBody) credentials: Credentials): Promise<{ token: string }> {
    const user = await this.userService.verifyCredentials(credentials);
    const userProfile = this.userService.convertToUserProfile(user);
    const token = await this.jwtService.generateToken(userProfile);

    return { token };
  }


  /**
   * Update the connected user only
   * @param currentUserProfile
   * @param user
   */
  @get('/users/me', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'User PATCH success',
        content: {
          'application/json': {
            schema: {
              'x-ts-type': User
            }
          }
        }
      }
    }
  })
  @authenticate('jwt')
  async getCurrentUser(
    @inject(SecurityBindings.USER)
    currentUserProfile: UserProfile
  ): Promise<User> {
    // Inclusion not working : Trpuble with mongo ObjectId
    const user = await this.userRepository.findById(currentUserProfile.id, { fields: { password: false } });
    const roles = await this.userRoleRepository.find({ where: { userId: { like: currentUserProfile.id } } });
    user.roles = roles;

    return user;
  }
}
