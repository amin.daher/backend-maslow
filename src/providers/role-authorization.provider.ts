import { Provider } from '@loopback/core';
import { Authorizer, AuthorizationContext, AuthorizationMetadata, AuthorizationDecision } from '@loopback/authorization';
import { repository } from '@loopback/repository';
import { UserRoleRepository } from '../repositories';

export class RoleAuthorizationProvider implements Provider<Authorizer> {
  constructor(
    @repository(UserRoleRepository) private userRoleRepository: UserRoleRepository,
  ) { }

  /**
   * @returns authenticateFn
   */
  value(): Authorizer {

    return this.authorize.bind(this);
  }

  async authorize(
    authorizationCtx: AuthorizationContext,
    metadata: AuthorizationMetadata,
  ) {
    const user = authorizationCtx.principals[0];
    const userRoles = await this.userRoleRepository.find({ where: { userId: { like: user.id } } });
    const allowedRoles = metadata.allowedRoles;
    if (allowedRoles) {
      if (userRoles) {
        for (let i = 0; i < userRoles.length; i++) {
          if (allowedRoles.includes(userRoles[i].roleId)) {

            return AuthorizationDecision.ALLOW;
          }
        }
      }
      return AuthorizationDecision.DENY;
    } else {

      return AuthorizationDecision.ALLOW;
    }
  }
}
