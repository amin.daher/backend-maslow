import { BackendMaslow } from './application';
import { ApplicationConfig } from '@loopback/core';

export { BackendMaslow };

export async function main(options: ApplicationConfig = {}) {
  const app = new BackendMaslow(options);
  await app.boot();
  app.migrateSchema();
  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);

  return app;
}
