export const API_URL = "openhab:8080/rest/";

export const OPENHAB_SERVER_HOST = 'openhab';
export const OPENHAB_SERVER_PORT = 8080;
export const THINGS_RESOURCE = '/rest/things/';
export const ITEMS_RESOURCE = '/rest/items/';
