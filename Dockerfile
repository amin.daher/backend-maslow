FROM node:10-slim

# Set to a non-root built-in user `node`
USER node

# Create app directory
RUN mkdir -p /home/node/app
WORKDIR /home/node/app

# Install app dependencies
COPY --chown=node package*.json ./
RUN npm install

# Copy app source code
COPY --chown=node . .

#Build app
RUN npm run build

# Serve App
CMD [ "node", "." ]
